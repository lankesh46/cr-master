<%@tag description="basepage" body-content="scriptless"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@attribute name="headContent" fragment="true" required="true"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>Dashboard | CRM</title>

<!--=== CSS ===-->

<!-- Bootstrap -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />

<!-- jQuery UI -->
<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

<!-- Theme -->
<link href="assets/css/main.css" rel="stylesheet" type="text/css" />
<link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
<link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet"
	href="assets/css/fontawesome/font-awesome.min.css">
<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'
	rel='stylesheet' type='text/css'>

<!--=== JavaScript ===-->

<script type="text/javascript" src="assets/js/libs/jquery-1.10.2.min.js"></script>
<script type="text/javascript"
	src="plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/libs/lodash.compat.min.js"></script>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

<!-- Smartphone Touch Events -->
<script type="text/javascript"
	src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript"
	src="plugins/event.swipe/jquery.event.move.js"></script>
<script type="text/javascript"
	src="plugins/event.swipe/jquery.event.swipe.js"></script>

<!-- General -->
<script type="text/javascript" src="assets/js/libs/breakpoints.js"></script>
<script type="text/javascript" src="plugins/respond/respond.min.js"></script>
<!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
<script type="text/javascript" src="plugins/cookie/jquery.cookie.min.js"></script>
<script type="text/javascript"
	src="plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript"
	src="plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

<!-- Page specific plugins -->
<!-- Charts -->
<!--[if lt IE 9]>
		<script type="text/javascript" src="plugins/flot/excanvas.min.js"></script>
	<![endif]-->
<script type="text/javascript"
	src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript"
	src="plugins/flot/jquery.flot.tooltip.min.js"></script>
<script type="text/javascript"
	src="plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript"
	src="plugins/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript"
	src="plugins/flot/jquery.flot.growraf.min.js"></script>
<script type="text/javascript"
	src="plugins/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<script type="text/javascript"
	src="plugins/daterangepicker/moment.min.js"></script>
<script type="text/javascript"
	src="plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
	src="plugins/blockui/jquery.blockUI.min.js"></script>
<script type="text/javascript" src="plugins/fileinput/fileinput.js"></script>


<script type="text/javascript"
	src="plugins/fullcalendar/fullcalendar.min.js"></script>

<!-- Form Validation -->
<script type="text/javascript"
	src="plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="plugins/validation/additional-methods.min.js"></script>


<!-- Noty -->
<script type="text/javascript" src="plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="plugins/noty/layouts/top.js"></script>
<script type="text/javascript" src="plugins/noty/themes/default.js"></script>

<!-- Forms -->
<script type="text/javascript"
	src="plugins/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="plugins/select2/select2.min.js"></script>

<!-- DataTables -->
	 <script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="plugins/datatables/tabletools/TableTools.min.js"></script>
	<script type="text/javascript" src="plugins/datatables/colvis/ColVis.min.js"></script> 
	<script type="text/javascript" src="plugins/datatables/DT_bootstrap.js"></script>

<!-- App -->
<script type="text/javascript" src="assets/js/app.js"></script>

<script type="text/javascript" src="assets/js/plugins.js"></script>
<script type="text/javascript"
	src="assets/js/plugins.form-components.js"></script>
	


<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
	});
	
	// redirect  the tab 
	
	
</script>

<!-- Demo JS -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript" src="assets/js/demo/pages_calendar.js"></script>
<script type="text/javascript"
	src="assets/js/demo/charts/chart_filled_blue.js"></script>
<script type="text/javascript"
	src="assets/js/demo/charts/chart_simple.js"></script>
<script type="text/javascript" src="assets/js/demo/form_validation.js"></script>s
<style type="text/css">
.alert {
padding: 5px !important;
margin-bottom: 20px;
border: 1px solid transparent;
border-radius: 4px;
margin-top: 4px !important;
}
</style>

<jsp:invoke fragment="headContent" />
</head>

<body>

	<!-- Header -->
	<header class="header navbar navbar-fixed-top" role="banner">
		<!-- Top Navigation Bar -->
		<div class="container" style="background-color: #f44300;">

			<!-- Only visible on smartphones, menu toggle -->
			<ul class="nav navbar-nav">
				<li class="nav-toggle"><a href="javascript:void(0);" title=""><i
						class="icon-reorder"></i></a></li>
			</ul>

			<!-- Logo -->
			<a class="navbar-brand" href="index.jsp"> <img
				src="assets/img/logo.png" alt="logo" /> <strong>CR-</strong>Master
			</a>
			<!-- /logo -->

			<!-- Sidebar Toggler -->
			<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom"
				data-original-title="Toggle navigation"> <i class="icon-reorder"></i>
			</a>
			<!-- /Sidebar Toggler -->

			<!-- Top Left Menu -->
			<ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">
				<li><a href="#"> Dashboard </a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> Dropdown <i
						class="icon-caret-down small"></i>
				</a>
					<ul class="dropdown-menu">
						<li><a href="#"><i class="icon-user"></i> Example #1</a></li>
						<li><a href="#"><i class="icon-calendar"></i> Example #2</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-tasks"></i> Example #3</a></li>
					</ul></li>
				<li><div id="msg" class="showmsg" style="margin-bottom: -20px !important;
margin-left: 6px !important;">
					<c:if test="${not empty requestScope.alertMessage}">
						<div class="alert alert-success fade in showmsg" id="msgAlert">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>Message !!</strong>
							<c:out value="${requestScope.alertMessage}"></c:out>
						</div>
					</c:if>
				</div>
				</li>
			</ul>
			<!-- /Top Left Menu -->

			<!-- Top Right Menu -->
			<ul class="nav navbar-nav navbar-right">
				<!-- Notifications -->
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> <i class="icon-warning-sign"></i> <span
						class="badge">5</span>
				</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 5 new notifications</p>
						</li>
						<li><a href="javascript:void(0);"> <span
								class="label label-success"><i class="icon-plus"></i></span> <span
								class="message">New user registration.</span> <span class="time">1
									mins</span>
						</a></li>
						<li><a href="javascript:void(0);"> <span
								class="label label-danger"><i class="icon-warning-sign"></i></span>
								<span class="message">High CPU load on cluster #2.</span> <span
								class="time">5 mins</span>
						</a></li>
						<li><a href="javascript:void(0);"> <span
								class="label label-success"><i class="icon-plus"></i></span> <span
								class="message">New user registration.</span> <span class="time">10
									mins</span>
						</a></li>
						<li><a href="javascript:void(0);"> <span
								class="label label-info"><i class="icon-bullhorn"></i></span> <span
								class="message">New items are in queue.</span> <span
								class="time">25 mins</span>
						</a></li>
						<li><a href="javascript:void(0);"> <span
								class="label label-warning"><i class="icon-bolt"></i></span> <span
								class="message">Disk space to 85% full.</span> <span
								class="time">55 mins</span>
						</a></li>
						<li class="footer"><a href="javascript:void(0);">View all
								notifications</a></li>
					</ul></li>

				<!-- Tasks -->
				<li class="dropdown hidden-xs hidden-sm">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i
						class="icon-tasks"></i> <span class="badge">7</span>
				</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 7 pending tasks</p>
						</li>
						<li><a href="javascript:void(0);"> <span class="task">
									<span class="desc">Preparing new release</span> <span
									class="percent">30%</span>
							</span>
								<div class="progress progress-small">
									<div style="width: 30%;" class="progress-bar progress-bar-info"></div>
								</div>
						</a></li>
						<li><a href="javascript:void(0);"> <span class="task">
									<span class="desc">Change management</span> <span
									class="percent">80%</span>
							</span>
								<div class="progress progress-small progress-striped active">
									<div style="width: 80%;"
										class="progress-bar progress-bar-danger"></div>
								</div>
						</a></li>
						<li><a href="javascript:void(0);"> <span class="task">
									<span class="desc">Mobile development</span> <span
									class="percent">60%</span>
							</span>
								<div class="progress progress-small">
									<div style="width: 60%;"
										class="progress-bar progress-bar-success"></div>
								</div>
						</a></li>
						<li><a href="javascript:void(0);"> <span class="task">
									<span class="desc">Database migration</span> <span
									class="percent">20%</span>
							</span>
								<div class="progress progress-small">
									<div style="width: 20%;"
										class="progress-bar progress-bar-warning"></div>
								</div>
						</a></li>
						<li class="footer"><a href="javascript:void(0);">View all
								tasks</a></li>
					</ul>
				</li>

				<!-- Messages -->
				<li class="dropdown hidden-xs hidden-sm"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <i
						class="icon-envelope"></i> <span class="badge">1</span>
				</a>
					<ul class="dropdown-menu extended notification">
						<li class="title">
							<p>You have 3 new messages</p>
						</li>
						<li><a href="javascript:void(0);"> <span class="photo"><img
									src="assets/img/demo/avatar-1.jpg" alt="" /></span> <span
								class="subject"> <span class="from">Bob Carter</span> <span
									class="time">Just Now</span>
							</span> <span class="text"> Consetetur sadipscing elitr... </span>
						</a></li>
						<li><a href="javascript:void(0);"> <span class="photo"><img
									src="assets/img/demo/avatar-2.jpg" alt="" /></span> <span
								class="subject"> <span class="from">Jane Doe</span> <span
									class="time">45 mins</span>
							</span> <span class="text"> Sed diam nonumy... </span>
						</a></li>
						<li><a href="javascript:void(0);"> <span class="photo"><img
									src="assets/img/demo/avatar-3.jpg" alt="" /></span> <span
								class="subject"> <span class="from">Patrick Nilson</span>
									<span class="time">6 hours</span>
							</span> <span class="text"> No sea takimata sanctus... </span>
						</a></li>
						<li class="footer"><a href="javascript:void(0);">View all
								messages</a></li>
					</ul></li>

				<!-- .row .row-bg Toggler -->
				<li><a href="#" class="dropdown-toggle row-bg-toggle"> <i
						class="icon-resize-vertical"></i>
				</a></li>

				<!-- Project Switcher Button -->
				<li class="dropdown" style="display: none"><a href="#"
					onclick="return false;"
					class="project-switcher-btn dropdown-toggle"> <i
						class="icon-folder-open"></i> <span>Projects</span>
				</a></li>

				<!-- User Login Dropdown -->
				<li class="dropdown user"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> <!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
						<i class="icon-male"></i> <span class="username"><c:out value="${usersession.employee.name}"></c:out></span> <i
						class="icon-caret-down small"></i>
				</a>
					<ul class="dropdown-menu">
						<li><a href="pages_user_profile.html"><i
								class="icon-user"></i> My Profile</a></li>
						<li><a href="pages_calendar.html"><i
								class="icon-calendar"></i> My Calendar</a></li>
						<li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>
						<li class="divider"></li>
						<li><a href="Logout"><i class="icon-key"></i> Log Out</a></li>
					</ul></li>
				<!-- /user login dropdown -->
			</ul>
			<!-- /Top Right Menu -->
		</div>
		<!-- /top navigation bar -->

		<!--=== Project Switcher ===-->
		<div id="project-switcher" class="container project-switcher">
			<div id="scrollbar">
				<div class="handle"></div>
			</div>

			<div id="frame">
				<ul class="project-list">
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-desktop"></i></span> <span class="title">Lorem ipsum
								dolor</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-compass"></i></span> <span class="title">Dolor sit
								invidunt</span>
					</a></li>
					<li class="current"><a href="javascript:void(0);"> <span
							class="image"><i class="icon-male"></i></span> <span
							class="title">Consetetur sadipscing elitr</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-thumbs-up"></i></span> <span class="title">Sed diam
								nonumy</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-female"></i></span> <span class="title">At vero eos
								et</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-beaker"></i></span> <span class="title">Sed diam
								voluptua</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-desktop"></i></span> <span class="title">Lorem ipsum
								dolor</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-compass"></i></span> <span class="title">Dolor sit
								invidunt</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-male"></i></span> <span class="title">Consetetur
								sadipscing elitr</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-thumbs-up"></i></span> <span class="title">Sed diam
								nonumy</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-female"></i></span> <span class="title">At vero eos
								et</span>
					</a></li>
					<li><a href="javascript:void(0);"> <span class="image"><i
								class="icon-beaker"></i></span> <span class="title">Sed diam
								voluptua</span>
					</a></li>
				</ul>
			</div>
			<!-- /#frame -->
		</div>
		<!-- /#project-switcher -->
	</header>
	<!-- /.header -->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">

				<!-- Search Input -->
				<form class="sidebar-search">
					<div class="input-box">
						<button type="submit" class="submit">
							<i class="icon-search"></i>
						</button>
						<span> <input type="text" placeholder="Search...">
						</span>
					</div>
				</form>

				<!-- Search Results -->
				<div class="sidebar-search-results">

					<i class="icon-remove close"></i>
					<!-- Documents -->
					<div class="title">Documents</div>
					<ul class="notifications">
						<li><a href="javascript:void(0);">
								<div class="col-left">
									<span class="label label-info"><i class="icon-file-text"></i></span>
								</div>
								<div class="col-right with-margin">
									<span class="message"><strong>John Doe</strong> received
										$1.527,32</span> <span class="time">finances.xls</span>
								</div>
						</a></li>
						<li><a href="javascript:void(0);">
								<div class="col-left">
									<span class="label label-success"><i
										class="icon-file-text"></i></span>
								</div>
								<div class="col-right with-margin">
									<span class="message">My name is <strong>John
											Doe</strong> ...
									</span> <span class="time">briefing.docx</span>
								</div>
						</a></li>
					</ul>
					<!-- /Documents -->
					<!-- Persons -->
					<div class="title">Persons</div>
					<ul class="notifications">
						<li><a href="javascript:void(0);">
								<div class="col-left">
									<span class="label label-danger"><i class="icon-female"></i></span>
								</div>
								<div class="col-right with-margin">
									<span class="message">Jane <strong>Doe</strong></span> <span
										class="time">21 years old</span>
								</div>
						</a></li>
					</ul>
				</div>
				<!-- /.sidebar-search-results -->

				<!--=== Navigation ===-->
				<ul id="nav">
					<li class="current"><a href="index.jsp"> <i
							class="icon-dashboard"></i> Dashboard
					</a></li>
					<li><a href="javascript:void(0);"> <i class="icon-desktop"></i>
							Create Master <span class="label label-info pull-right">6</span>
					</a>
						<ul class="sub-menu">
							<li><a href="company.jsp"> <i
									class="icon-angle-right"></i>Company Profile
							</a></li>
							<li><a href="officeLocation.jsp"> <i
									class="icon-angle-right"></i>Office Location
							</a></li>
							<li><a href="designations.jsp"> <i
									class="icon-angle-right"></i>Designations
							</a></li>
							<li><a href="clientgroup.jsp"> <i
									class="icon-angle-right"></i>Client Group
							</a></li>
							<li><a href="banks.jsp"> <i
									class="icon-angle-right"></i>Banks
							</a></li>
							<li><a href="clients.jsp"> <i
									class="icon-angle-right"></i>Clients
							</a></li>
							<li><a href="employee.jsp"> <i
									class="icon-angle-right"></i>Employee
							</a></li>
							<li><a href="team.jsp"> <i class="icon-angle-right"></i>
									Team
							</a></li>
							<li><a href="projectFeeding.jsp"> <i class="icon-angle-right"></i>
									Project Feeding
							</a></li>
						</ul></li>
					<li style="display:none"><a href="javascript:void(0);"> <i class="icon-edit"></i>
							Form Elements
					</a>
						<ul class="sub-menu">
							<li><a href="form_components.html"> <i
									class="icon-angle-right"></i> Form Components
							</a></li>
							<li><a href="form_layouts.html"> <i
									class="icon-angle-right"></i> Form Layouts
							</a></li>
							<li><a href="form_validation.html"> <i
									class="icon-angle-right"></i> Form Validation
							</a></li>
						</ul></li>
					<li style="display:none"><a href="javascript:void(0);"> <i class="icon-table"></i>
							Tables
					</a>
						<ul class="sub-menu">
							<li><a href="tables_static.html"> <i
									class="icon-angle-right"></i> Static Tables
							</a></li>
							<li><a href="tables_dynamic.html"> <i
									class="icon-angle-right"></i> Dynamic Tables (DataTables)
							</a></li>
							<li><a href="tables_responsive.html"> <i
									class="icon-angle-right"></i> Responsive Tables
							</a></li>
						</ul></li>
					<li style="display:none"><a href="javascript:void(0);"> <i
							class="icon-folder-open-alt"></i> Pages
					</a>
						<ul class="sub-menu">
							<li><a href="login.html"> <i class="icon-angle-right"></i>
									Login
							</a></li>
							<li><a href="pages_user_profile.html"> <i
									class="icon-angle-right"></i> User Profile
							</a></li>
							<li><a href="pages_calendar.html"> <i
									class="icon-angle-right"></i> Calendar
							</a></li>
							<li><a href="pages_invoice.html"> <i
									class="icon-angle-right"></i> Invoice
							</a></li>
						</ul></li>
					<li style="display:none"><a href="javascript:void(0);"> <i class="icon-list-ol"></i>
							4 Level Menu
					</a>
						<ul class="sub-menu">
							<li class="open-default"><a href="javascript:void(0);">
									<i class="icon-cogs"></i> Item 1 <span class="arrow"></span>
							</a>
								<ul class="sub-menu">
									<li class="open-default"><a href="javascript:void(0);">
											<i class="icon-user"></i> Sample Link 1 <span class="arrow"></span>
									</a>
										<ul class="sub-menu">
											<li class="current"><a href="javascript:void(0);"><i
													class="icon-remove"></i> Sample Link 1</a></li>
											<li><a href="javascript:void(0);"><i
													class="icon-pencil"></i> Sample Link 1</a></li>
											<li><a href="javascript:void(0);"><i
													class="icon-edit"></i> Sample Link 1</a></li>
										</ul></li>
									<li><a href="javascript:void(0);"><i class="icon-user"></i>
											Sample Link 1</a></li>
									<li><a href="javascript:void(0);"><i
											class="icon-external-link"></i> Sample Link 2</a></li>
									<li><a href="javascript:void(0);"><i class="icon-bell"></i>
											Sample Link 3</a></li>
								</ul></li>
							<li><a href="javascript:void(0);"> <i class="icon-globe"></i>
									Item 2 <span class="arrow"></span>
							</a>
								<ul class="sub-menu">
									<li><a href="javascript:void(0);"><i class="icon-user"></i>
											Sample Link 1</a></li>
									<li><a href="javascript:void(0);"><i
											class="icon-external-link"></i> Sample Link 1</a></li>
									<li><a href="javascript:void(0);"><i class="icon-bell"></i>
											Sample Link 1</a></li>
								</ul></li>
							<li><a href="javascript:void(0);"> <i
									class="icon-folder-open"></i> Item 3
							</a></li>
						</ul></li>
				</ul>

				<!-- /Navigation -->
				<div class="sidebar-title" style="display: none">
					<span>Notifications</span>
				</div>


				<div class="sidebar-widget align-center">
					<div class="btn-group" data-toggle="buttons" id="theme-switcher">
						<label class="btn active"> <input type="radio"
							name="theme-switcher" data-theme="bright"><i
							class="icon-sun"></i> Bright
						</label> <label class="btn"> <input type="radio"
							name="theme-switcher" data-theme="dark"><i
							class="icon-moon"></i> Dark
						</label>
					</div>
				</div>

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->

		<div id="content">
			<div class="container">
				<!-- Breadcrumbs line -->
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li><i class="icon-home"></i> <a href="index.jsp">Dashboard</a>
						</li>
						<li class="current"><a href="pages_calendar.html" title="">Calendar</a>
						</li>
					</ul>

					<ul class="crumb-buttons">
						<li style="display:none"><a href="charts.html" title=""><i class="icon-signal"></i><span>Statistics</span></a></li>
						<li class="dropdown"><a href="#" title=""
							data-toggle="dropdown"><i class="icon-tasks"></i><span>Users
									<strong>(+3)</strong>
							</span><i class="icon-angle-down left-padding"></i></a>
							<ul class="dropdown-menu pull-right">
								<li><a href="form_components.html" title=""><i
										class="icon-plus"></i>Add new User</a></li>
								<li><a href="tables_dynamic.html" title=""><i
										class="icon-reorder"></i>Overview</a></li>
							</ul></li>
						<li class="range"><a href="#"> <i class="icon-calendar"></i>
								<span></span> <i class="icon-angle-down"></i>
						</a></li>
					</ul>
				</div>
				<!-- /Breadcrumbs line -->

				<!--=== Page Header ===-->
				<div class="page-header" style="display:none">
					<div class="page-title">
						<h3>Welcome</h3>
						<!-- <span>Good morning, John!</span> -->
					</div>

					<!-- Page Stats -->
					<!-- /Page Stats -->
				</div>
				<!-- /Page Header -->

				<!--=== Page Content ===-->
				<!--=== Statboxes ===-->
				<div id="main">
					<jsp:doBody /><%-- output anything defined in our JSP inside <jsp:body> tag --%>
				</div>
				<!-- /Statboxes -->

				<!--=== Blue Chart ===-->
				<div class="row" style="display: none">
					<div class="col-md-12"></div>
					<!-- /.col-md-12 -->
				</div>
				<!-- /.row -->
				<!-- /Blue Chart -->


				<div class="row" style="display: none">
					<!--=== Calendar ===-->

					<!-- /Calendar -->

					<!--=== Feeds (with Tabs) ===-->
					<div class="col-md-6">
						<div class="widget">

							<div class="widget-content">
								<div class="tabbable tabbable-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab_feed_1"
											data-toggle="tab">System</a></li>
										<li><a href="#tab_feed_2" data-toggle="tab">Activities</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab_feed_1">
											<div class="scroller" data-height="290px"
												data-always-visible="1" data-rail-visible="0">
												<ul class="feeds clearfix">
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-bell"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">You have 2 puzzles to solve.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">Just now</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New user registered.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">20 mins ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li class="hoverable"><a href="javascript:void(0);">
															<div class="col1">
																<div class="content">
																	<div class="content-col1">
																		<div class="label label-info">
																			<i class="icon-bullhorn"></i>
																		</div>
																	</div>
																	<div class="content-col2">
																		<div class="desc">New items are in queue.</div>
																	</div>
																</div>
															</div> <!-- /.col1 -->
															<div class="col2">
																<div class="date">25 mins ago</div>
															</div> <!-- /.col2 -->
													</a></li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-danger">
																		<i class="icon-warning-sign"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">
																		High CPU load on cluster #2. <span
																			class="label label-danger label-mini">Fix it <i
																			class="icon-share-alt"></i></span>
																	</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">30 mins ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-warning">
																		<i class="icon-bolt"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">Disk space to 85% full.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">45 mins ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New user registered.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">1 hour ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-default">
																		<i class="icon-time"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">Time successfully synced.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">1.5 hours ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-info">
																		<i class="icon-bullhorn"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">Download finished.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">1.8 hours ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New order received.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">2 hours ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-info">
																		<i class="icon-bullhorn"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">Download finished.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">3 hours ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New order received.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">5 hours ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-info">
																		<i class="icon-bullhorn"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">Download finished.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">5.5 hours ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New order received.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">7 hours ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-info">
																		<i class="icon-bullhorn"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">Download finished.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">16 hours ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New order received.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">20 hours ago</div>
														</div> <!-- /.col2 -->
													</li>
												</ul>
												<!-- /.feeds -->
											</div>
											<!-- /.scroller -->
										</div>
										<!-- /#tab_feed_1 -->

										<div class="tab-pane" id="tab_feed_2">
											<div class="scroller" data-height="290px"
												data-always-visible="1" data-rail-visible="0">
												<ul class="feeds clearfix">
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New user registered.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">1 min ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New user registered.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">5 mins ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-info">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New order received.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">10 mins ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New user registered.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">20 mins ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-info">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New order received.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">30 mins ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New user registered.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">40 mins ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-info">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New order received.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">50 mins ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-success">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New user registered.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">1 hour ago</div>
														</div> <!-- /.col2 -->
													</li>
													<li>
														<div class="col1">
															<div class="content">
																<div class="content-col1">
																	<div class="label label-info">
																		<i class="icon-plus"></i>
																	</div>
																</div>
																<div class="content-col2">
																	<div class="desc">New order received.</div>
																</div>
															</div>
														</div> <!-- /.col1 -->
														<div class="col2">
															<div class="date">1.5 hours ago</div>
														</div> <!-- /.col2 -->
													</li>
												</ul>
												<!-- /.feeds -->
											</div>
											<!-- /.scroller -->
										</div>
										<!-- /#tab_feed_1 -->
									</div>
									<!-- /.tab-content -->
								</div>
								<!-- /.tabbable tabbable-custom-->
							</div>
							<!-- /.widget-content -->
						</div>
						<!-- /.widget .box -->
					</div>
					<!-- /.col-md-6 -->
					<!-- /Feeds (with Tabs) -->
				</div>
				<!-- /.row -->
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	</div>
<script type="text/javascript">
$(window).load(function() {
        $("#msg").fadeTo(2000, 500).slideUp(500, function(){
       	$("#msg").slideUp(500);
        });   
});
</script>
</body>
</html>