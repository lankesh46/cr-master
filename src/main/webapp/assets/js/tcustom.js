 $(document).ready(function() {
		$('#myTab a').click(function(e) {
			e.preventDefault();
			$(this).tab('show');
		});

		// store the currently selected tab in the window.name which is stabled till
		// the window is closed
		$("ul.nav-tabs > li.tb-list > a").on("shown.bs.tab", function(e) {
			var id = $(e.target).attr("href").substr(1);
			window.name = id;
		});
		var hashID = "#" + window.name;
		console.log("HASH ID ---"+hashID);
		// on load of the page: switch to the currently selected tab
		$('#myTab a[href= "' + hashID + '"]').tab('show');
		console.log(hashID);
	});