<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags/mypagetemplates"%>
<template:page>
	<jsp:attribute name="headContent">
    </jsp:attribute>
	<jsp:body>
    
    <script type="text/javascript">
    function deleteTeamData(action,id)
    {
    	$.post('TeamServlet', {
            id : id,
            action : action
            
    }, function(responseText) {
        location.reload();   
    });
    }
    </script>
    
    <div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4>
							<i class="icon-reorder"></i> Team </h4>
							</div>
							<div class="widget-content">
								<div class="tabbable box-tabs">
									<ul class="nav nav-tabs">
										<!-- <li><a href="#box_tab3" data-toggle="tab">Section 3</a></li> -->
										<li><a href="#box_tab2" data-toggle="tab">Team Details</a></li>
										<li class="active"><a href="#box_tab1" data-toggle="tab">Team </a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="box_tab1">
											<!--Section 1 Start  -->
									<div class="row">
					<!--=== Validation Example 1 ===-->
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header" style="display: none">
								<h4>
														<i class="icon-reorder"></i> Team </h4>
							</div>
							<div class="widget-content">
								<form class="form-horizontal" id="validate-1" action="TeamServlet">
								<div class="row">
									<div class="form-group col-md-6">
										<label class="col-md-3 control-label"> Name <span
																	class="required">*</span></label>
										<div class="col-md-9">
											<input type="text" name="name" class="form-control required"
																		required>
										</div>
									</div>
									
									
									<div class="form-group col-md-6">
										<label class="col-md-3 control-label"> Members <span
																	class="required">*</span></label>
										<div class="col-md-9">
										<select name="member_empId" class="form-control required" multiple required>
												<option value="">select...</option>
					                            <c:forEach
																			items="${dropdown.employeedata}" var="employeeData">
					                                  <option
																				value="${employeeData.key}"><c:out
																					value="${employeeData.value}"></c:out></option>  
                                 					 </c:forEach>  	
											</select>
											<!-- <input type="text" name="member_empId" class="form-control required" required> -->
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="form-group col-md-6">
										<label class="col-md-3 control-label"> Organization Level <span
																	class="required">*</span></label>
										<div class="col-md-9">
											<input type="text" name="org_level"
																		class="form-control required" required>
										</div>
									</div>
									<div class="form-group col-md-6">
										<label class="col-md-3 control-label"> Location <span
																	class="required">*</span></label>
										<div class="col-md-9">
										<select name="locationId" class="form-control required" required>
												<option value="">select...</option>
					                            <c:forEach
																			items="${dropdown.officeLoc}" var="officesloc">
					                                  <option value="${officesloc.key}"><c:out
																					value="${officesloc.value}"></c:out></option>  
                                 					 </c:forEach>  	
											</select>
											<!-- <input type="text" name="locationId" class="form-control required" required> -->
										</div>
									</div>
								</div>
								
								<div class="row" style="display:none;">
									<div class="form-group col-md-6">
										<label class="col-md-3 control-label"> Email <span
																	class="required">*</span></label>
										<div class="col-md-9">
											<input type="email" name="email"
																		class="form-control required"  style="display: 'none'">
										</div>
									</div>
									
								</div>
							
									
									<div class="form-actions">
										<center>
																<button type="submit" name="action" value="GENERATED"
																	id="submit" class="btn btn-primary ">Submit</button>
															</center>
									</div>
								</form>
							</div>
						</div>
						<!-- /Validation Example 1 -->
					</div>

				
				</div>
											<!--Section 1 End -->
										</div>
										<div class="tab-pane" id="box_tab2">
											<!--Section 2 Start  -->
											<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header " style="display: none">
								<h4>
														<i class="icon-reorder"></i> Horizontal Scrolling (<code>no-padding</code>)</h4>
								<div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i
																class="icon-angle-down"></i></span>
									</div>
								</div>
							</div>
							<div class="widget-content no-padding">
								<div id="DataTables_Table_4_wrapper"
														class="dataTables_wrapper form-inline" role="grid">
													
														<div class="dataTables_scroll">
															
															<div class="row"
																style="overflow: auto; width: 100%;">
																<table
																	class="table table-striped table-bordered table-hover table-checkable datatable">
														
									<thead>
										<tr role="row">
																			<th class="checkbox-column sorting_disabled"
																				role="columnheader" rowspan="1" colspan="1"
																				aria-label="
												
											"
																				style="width: 19px;">
												<div class="checker">
																					<span><input type="checkbox" class="uniform"></span>
																				</div>
											</th>
																			<th class="sorting" role="columnheader" tabindex="0"
																				aria-controls="DataTables_Table_4" rowspan="1"
																				colspan="1"
																				aria-label="First Name: activate to sort column ascending"
																				style="width: 369px;">Serial No.</th>
																			<th class="sorting" role="columnheader" tabindex="0"
																				aria-controls="DataTables_Table_4" rowspan="1"
																				colspan="1"
																				aria-label="Last Name: activate to sort column ascending"
																				style="width: 359px;"> Team Name </th>
																			<th class="hidden-xs sorting" role="columnheader"
																				tabindex="0" aria-controls="DataTables_Table_4"
																				rowspan="1" colspan="1"
																				aria-label="Username: activate to sort column ascending"
																				style="width: 347px;">Organization Level</th>
																			<th class="sorting" role="columnheader" tabindex="0"
																				aria-controls="DataTables_Table_4" rowspan="1"
																				colspan="1"
																				aria-label="Status: activate to sort column ascending"
																				style="width: 342px;">Status</th>
																		</tr>
									</thead>
										
										<tbody role="alert" aria-live="polite" aria-relevant="all">
										 <c:forEach var="master"
																			items="${requestBean.masterTeam}">
										 	<tr class="even">
										 	<td class="checkbox-column  sorting_1">
												<div class="checker">
																						<span><input type="checkbox"
																							class="uniform"></span>
																					</div>
											</td>
											<td class=" "><c:out value="${master['ID']}"></c:out></td>
											<td class=" "><c:out value="${master['Name']}"></c:out></td>
											<td class=" "><c:out value="${master['Orgnization Level']}"></c:out></td>
											<td class=" ">
											<%-- <span class="label label-info"><c:out
																							value="${master['Status']}"></c:out></span> --%>
											<button type="button" name="action" value="DELETE"
																						id="delete" class="btn btn-danger label label-info"
																						onclick="deleteTeamData('delete',${master['ID']})">Delete</button>
																							
											</td>
										 
																		
																		</c:forEach>
										</tr>
																	</tbody>
										</table>
															</div>
														</div>
														
													</div>
							</div>
						</div>
					</div>
				</div>
											<!--Section 2 End  -->
										</div>
									</div>
								</div> <!-- /.tabbable portlet-tabs -->
							</div> <!-- /.widget-content -->
						</div> <!-- /.widget .box -->
					</div> <!-- /.col-md-12 -->
				</div>
    <!--Older Jsp Start  -->	
	
 </jsp:body>
</template:page>