<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags/mypagetemplates"%>
<template:page>
	<jsp:attribute name="headContent">
    </jsp:attribute>
	<jsp:body>
    
    <script type="text/javascript">
    function deleteProjectFeedingData(action,id)
    {
    	$.post('ProjectFeedingServlet', {
            id : id,
            action : action
            
    }, function(responseText) {
        location.reload();   
    });
    }
    </script>
    
     <script type="text/javascript">
    function myFunction()
    {
    	var data = $("#negativeBanks").val();
    	
    	if(data == "" || data == null)
    	{
    		document.getElementById("ReasonDiv").style.visibility = "hidden";
    	}
    	else
    	{
    		document.getElementById("ReasonDiv").style.visibility = "visible";
    		
    		$("input:hidden").val("1");
    		
    		var reason = $('#reason');
    		
    		var hiddenValue = $('#result').val();
    		alert(hiddenValue);
    		if(hiddenValue === 1 || hiddenValue === "1" )
    		{
    				//reason.attr('required',true);
    			//	document.getElementById("reason").setAttribute('required','required');		
    		}
    		else
    		{
    			//document.getElementById("reason").removeAttribute('required');
    				//$("reason").removeAttr("required");
    		}
    		
        	
    		
    	}
    }
    </script>
    
    <div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4>
							<i class="icon-reorder"></i> Project Feeding </h4>
							</div>
							<div class="widget-content">
								<div class="tabbable box-tabs">
									<ul class="nav nav-tabs">
										<!-- <li><a href="#box_tab3" data-toggle="tab">Section 3</a></li> -->
										<li><a href="#box_tab2" data-toggle="tab">Project Feeding Details </a></li>
										<li class="active"><a href="#box_tab1" data-toggle="tab">Project Feeding </a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="box_tab1">
											<!--Section 1 Start  -->
									<div class="row">
					<!--=== Validation Example 1 ===-->
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header" style="display: none">
								<h4>
														<i class="icon-reorder"></i> Project Feeding </h4>
							</div>
							<div class="widget-content">
								<form class="form-horizontal" id="validate-1" action="ProjectFeedingServlet">
								<div class="row">
									<div class="form-group col-md-6">
										<label class="col-md-3 control-label"> Client Name <span
																	class="required">*</span></label>
										<div class="col-md-9">
										
										<select name="clientName" class="form-control required" required>
												<option value="">select...</option>
					                            <c:forEach
																			items="${dropdown.clientData}" var="bankData">
					                                  <option
																				value="${bankData.key}"><c:out
																					value="${bankData.value}"></c:out></option>  
                                 					 </c:forEach>  	
											</select>
											
										<!-- 	<input type="text" name="clientName" class="form-control required"
																		required> -->
										</div>
									</div>
									
									
									<div class="form-group col-md-6">
										<label class="col-md-3 control-label"> Positive Banks(Banks to be worked on) <span
																	class="required">*</span></label>
										<div class="col-md-9">
										<select name="positiveBanks" class="form-control required" multiple="multiple" required>
												<option value="">select...</option>
					                            <c:forEach
																			items="${dropdown.bankdata}" var="bankData">
					                                  <option
																				value="${bankData.key}"><c:out
																					value="${bankData.value}"></c:out></option>  
                                 					 </c:forEach>  	
											</select>
											<!-- <input type="text" name="member_empId" class="form-control required" required> -->
										</div>
									</div>
								</div>
								
								<div class="row">
									
									<div class="form-group col-md-6">
										<label class="col-md-3 control-label"> Negative Banks( Client denied not to go ) <span
																	class="required">*</span></label>
										<div class="col-md-9">
										<select name="negativeBanks" id = "negativeBanks" class="form-control required" multiple = "multiple" onblur="myFunction()" required>
												<option value="">select...</option>
					                            <c:forEach
																			items="${dropdown.bankdata}" var="bankData">
					                                  <option
																				value="${bankData.key}"><c:out
																					value="${bankData.value}"></c:out></option>  
                                 					 </c:forEach>  
											</select>
											<!-- <input type="text" name="locationId" class="form-control required" required> -->
										</div>
									</div>
									
									<div class="form-group col-md-6" id ="ReasonDiv" style = "visibility:hidden;">
										<label class="col-md-3 control-label"> Reason for Selecting Negative Bank <span
																	class="required">*</span></label>
										<div class="col-md-9">
											<textarea rows="2" cols="10" class = "form-control" name = "reason" id = "reason"></textarea>
										</div>
									</div>
								</div>
								
								<div class="row">
								
									<div class="form-group col-md-6">
										<label class="col-md-3 control-label"> Start Date <span
																	class="required">*</span></label>
										<div class="col-md-9">
											<input type="date" name="startDate"
																		class="form-control required" required>
											<input type="hidden" name="result" value = "0"
																		class="form-control " >
										</div>
									</div>
						
								</div>
									<div class="form-actions">
										<center>
																<button type="submit" name="action" value="GENERATED"
																	id="submit" class="btn btn-primary " >Submit</button>
															</center>
									</div>
								</form>
							</div>
						</div>
						<!-- /Validation Example 1 -->
					</div>

				
				</div>
											<!--Section 1 End -->
										</div>
										<div class="tab-pane" id="box_tab2">
											<!--Section 2 Start  -->
											<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header " style="display: none">
								<h4>
														<i class="icon-reorder"></i> Horizontal Scrolling (<code>no-padding</code>)</h4>
								<div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i
																class="icon-angle-down"></i></span>
									</div>
								</div>
							</div>
							<div class="widget-content no-padding">
								<div id="DataTables_Table_4_wrapper"
														class="dataTables_wrapper form-inline" role="grid">
													
														<div class="dataTables_scroll">
															
															<div class="row"
																style="overflow: auto; width: 100%;">
																<table
																	class="table table-striped table-bordered table-hover table-checkable datatable">
														
									<thead>
										<tr role="row">
																			<th class="checkbox-column sorting_disabled"
																				role="columnheader" rowspan="1" colspan="1"
																				aria-label="
												
											"
																				style="width: 19px;">
												<div class="checker">
																					<span><input type="checkbox" class="uniform"></span>
																				</div>
											</th>
																			<th class="sorting" role="columnheader" tabindex="0"
																				aria-controls="DataTables_Table_4" rowspan="1"
																				colspan="1"
																				aria-label="First Name: activate to sort column ascending"
																				style="width: 369px;">Serial No.</th>
																			<th class="sorting" role="columnheader" tabindex="0"
																				aria-controls="DataTables_Table_4" rowspan="1"
																				colspan="1"
																				aria-label="Last Name: activate to sort column ascending"
																				style="width: 359px;"> Client Name </th>
																			<th class="hidden-xs sorting" role="columnheader"
																				tabindex="0" aria-controls="DataTables_Table_4"
																				rowspan="1" colspan="1"
																				aria-label="Username: activate to sort column ascending"
																				style="width: 347px;">Status</th>
																			<th class="sorting" role="columnheader" tabindex="0"
																				aria-controls="DataTables_Table_4" rowspan="1"
																				colspan="1"
																				aria-label="Status: activate to sort column ascending"
																				style="width: 342px;">Status</th>
																		</tr>
									</thead>
										
										<tbody role="alert" aria-live="polite" aria-relevant="all">
										 <c:forEach var="master"
																			items="${requestBean.masterProjectFeed}">
										 	<tr class="even">
										 	<td class="checkbox-column  sorting_1">
												<div class="checker">
																						<span><input type="checkbox"
																							class="uniform"></span>
																					</div>
											</td>
											<td class=" "><c:out value="${master['ID']}"></c:out></td>
											<td class=" "><c:out value="${master['Name']}"></c:out></td>
											<td class=" "><c:out value="${master['Status']}"></c:out></td>
											<td class=" ">
											<%-- <span class="label label-info"><c:out
																							value="${master['Status']}"></c:out></span> --%>
											<button type="button" name="action" value="DELETE"
																						id="delete" class="btn btn-danger label label-info"
																						onclick="deleteProjectFeedingData('delete',${master['ID']})">Delete</button>
																							
											</td>
										 
																		
																		</c:forEach>
										</tr>
																	</tbody>
										</table>
															</div>
														</div>
														
													</div>
							</div>
						</div>
					</div>
				</div>
											<!--Section 2 End  -->
										</div>
									</div>
								</div> <!-- /.tabbable portlet-tabs -->
							</div> <!-- /.widget-content -->
						</div> <!-- /.widget .box -->
					</div> <!-- /.col-md-12 -->
				</div>
    <!--Older Jsp Start  -->	
	
 </jsp:body>
</template:page>