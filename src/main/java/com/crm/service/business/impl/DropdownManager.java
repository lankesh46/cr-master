package com.crm.service.business.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.crm.persistence.CrudService;
import com.crm.persistence.QueryParameter;
import com.crm.persistence.entity.Bank;
import com.crm.persistence.entity.Client;
import com.crm.persistence.entity.Clientgroup;
import com.crm.persistence.entity.Employee;
import com.crm.persistence.entity.OfficeLocation;
import com.crm.service.business.contract.DropdownManagerLocal;

/**
 * Session Bean implementation class DropdownManager
 */
@Stateless
@LocalBean
public class DropdownManager implements DropdownManagerLocal
{

	@Inject
	CrudService crudService;

	/**
	 * Default constructor.
	 */
	public DropdownManager()
	{
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<String> officeLocation()
	{
		List<String> getOfficeLocation = crudService.findWithNamedQuery(
				"OfficeLocation.findAllByActive",
				QueryParameter.with("active", "active").parameters());
		return getOfficeLocation;
	}

	@Override
	public List<String> getAllDesignations()
	{
		List<String> allDesignations = crudService.findWithNamedQuery("Designation.findAll");
		return allDesignations;
	}

	@SuppressWarnings("null")
	@Override
	public Map<String, String> getClientGroup()
	{
		List<String> clientGroups = new ArrayList<String>();

		Map<String, String> clientGrp = new HashMap<String, String>();
		List<Clientgroup> fetchClientGroup = crudService.findWithNamedQuery("Clientgroup.findAll");

		if (!fetchClientGroup.isEmpty() && fetchClientGroup != null)
		{
			String clientName = "";
			String clientId = "";
			int setIndex = 0;
			for (Clientgroup clientgroup : fetchClientGroup)
			{

				if (!clientgroup.getName().isEmpty() && clientgroup.getName() != null)
				{
					clientName = clientgroup.getName().toString();
					setIndex++;
					//					clientGroups.add(clientName);
				}
				if (!Integer.toString(clientgroup.getId()).isEmpty()
						&& Integer.toString(clientgroup.getId()) != null)
				{
					clientId = Integer.toString(clientgroup.getId());
					setIndex++;
				}

				if (setIndex == 2)
				{
					clientGrp.put(clientId, clientName);
					setIndex = 0;
				}

			}
		}

		return clientGrp;
	}

	@Override
	public Map<String, String> getOfficeLoc()
	{

		Map<String, String> officeLoc = new HashMap<String, String>();
		List<OfficeLocation> fetchOfficeLoc = crudService
				.findWithNamedQuery("OfficeLocation.findAllByActive",QueryParameter.with("active", "active").parameters());
		if (!fetchOfficeLoc.isEmpty() && fetchOfficeLoc != null)
		{
			String OfficeName = "";
			String OfcLocId = "";
			int setIndex = 0;
			for (OfficeLocation officeloc : fetchOfficeLoc)
			{
				if (!officeloc.getName().isEmpty() && officeloc.getName() != null)
				{
					OfficeName = officeloc.getName() + "," + officeloc.getAddressStreet1() + ","
							+ officeloc.getAddressStreet2() + "," + officeloc.getZipcode() + ","
							+ officeloc.getCity() + "," + officeloc.getCountry();
					setIndex++;
				}
				if (!Integer.toString(officeloc.getId()).isEmpty()
						&& Integer.toString(officeloc.getId()) != null)
				{
					OfcLocId = Integer.toString(officeloc.getId());
					setIndex++;
				}
				if (setIndex == 2)
				{
					officeLoc.put(OfcLocId, OfficeName);
					setIndex = 0;
				}
			}
		}
		return officeLoc;
	}

	@Override
	public Map<String, String> getEmployeeData() {
		
		Map<String, String> employeeData = new HashMap<String, String>();
		List<Employee> employeeDetails = crudService
				.findWithNamedQuery("Employee.findAllByStatus",QueryParameter.with("active", "active").parameters());
		if (!employeeDetails.isEmpty() && employeeDetails != null)
		{
			String employeeName = "";
			String employeeId = "";
			int setIndex = 0;
			for (Employee employee : employeeDetails)
			{
				if (!employee.getName().isEmpty() && employee.getName() != null)
				{
					employeeName = employee.getName() + "";
					setIndex++;
				}
				if (!Integer.toString(employee.getId()).isEmpty()
						&& Integer.toString(employee.getId()) != null)
				{
					employeeId = Integer.toString(employee.getId());
					setIndex++;
				}
				if (setIndex == 2)
				{
					employeeData.put(employeeId, employeeName);
					setIndex = 0;
				}
			}
		}
		return employeeData;
	}

	@Override
	public Map<String, String> getBankData() {
		
		Map<String, String> bankData = new HashMap<String, String>();
		List<Bank> bankDetails = crudService
				.findWithNamedQuery("Bank.findByStatus",QueryParameter.with("active", "active").parameters());
		if (!bankDetails.isEmpty() && bankDetails != null)
		{
			String bankName = "";
			String bankId = "";
			int setIndex = 0;
			for (Bank bank : bankDetails)
			{
				if (!bank.getName().isEmpty() && bank.getName() != null)
				{
					bankName = bank.getName() + "";
					setIndex++;
				}
				if (!Integer.toString(bank.getId()).isEmpty()
						&& Integer.toString(bank.getId()) != null)
				{
					bankId = Integer.toString(bank.getId());
					setIndex++;
				}
				if (setIndex == 2)
				{
					bankData.put(bankId, bankName);
					setIndex = 0;
				}
			}
		}
		return bankData;
	}

	@Override
	public Map<String, String> getClientData() {
		
		Map<String, String> clientData = new HashMap<String, String>();
		List<Client> clientDetails = crudService
				.findWithNamedQuery("Client.findAllByStatus",QueryParameter.with("active", "active").parameters());
		if (!clientDetails.isEmpty() && clientDetails != null)
		{
			String clientName = "";
			String clientId = "";
			int setIndex = 0;
			for (Client client : clientDetails)
			{
				if (!client.getName().isEmpty() && client.getName() != null)
				{
					clientName = client.getName() + "";
					setIndex++;
				}
				if (!Integer.toString(client.getId()).isEmpty()
						&& Integer.toString(client.getId()) != null)
				{
					clientId = Integer.toString(client.getId());
					setIndex++;
				}
				if (setIndex == 2)
				{
					clientData.put(clientId, clientName);
					setIndex = 0;
				}
			}
		}
		return clientData;

	}

}
