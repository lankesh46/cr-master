package com.crm.service.business.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.crm.persistence.CrudService;
import com.crm.persistence.QueryParameter;
import com.crm.persistence.entity.Employee;
import com.crm.service.business.contract.EAuthService;
import com.crm.views.SessionBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author manoj
 *
 */
@Transactional
@Stateless
public class EAuthServiceImpl implements EAuthService
{

	/**
	 * 
	 */
	@Inject
	CrudService		crudService;

	@Inject
	SessionBean		userSession;
	
	private Logger	log	= LoggerFactory.getLogger(getClass());

	public EAuthServiceImpl()
	{
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isAuthenticated(String email)
	{
		if (!email.isEmpty() || email != null)
		{
			//			List<Employee> empData = entityManager.createNamedQuery("Employee.empByEmail")
			//					.setParameter("emailId", email).getResultList();
			List<Employee> empData = crudService.findWithNamedQuery("Employee.empByEmail",
					QueryParameter.with("emailId", email).parameters());
			if (!empData.isEmpty() && empData.size() > 0)
			{
				for (Employee employee : empData)
				{
					if (employee.getEmail().equalsIgnoreCase(email))
						return true;
				}
			}
		}
		log.debug("Authentication falied due to invalid employee email id {}");
		return false;
	}

	@Override
	public Employee setEmpData(String email)
	{
		Employee activeEmp = null ;
		if (!email.isEmpty() || email != null)
		{
			
			List<Employee> empData = crudService.findWithNamedQuery("Employee.empByEmail",
					QueryParameter.with("emailId", email).parameters());
			if (!empData.isEmpty() && empData.size() > 0)
			{
				for (Employee employee : empData)
				{
					if (employee.getEmail().equalsIgnoreCase(email))
						activeEmp = employee;
						//Code for Setting Logged Employee 
				}
			}
		}
		return activeEmp;
	}
	
	

}
