/**
 * 
 */
package com.crm.service.business.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.transaction.Transactional;

import com.crm.persistence.CrudService;
import com.crm.persistence.QueryParameter;
import com.crm.persistence.entity.Bank;
import com.crm.persistence.entity.Client;
import com.crm.persistence.entity.Clientgroup;
import com.crm.persistence.entity.CompanyProfile;
import com.crm.persistence.entity.Designation;
import com.crm.persistence.entity.Employee;
import com.crm.persistence.entity.OfficeLocation;
import com.crm.persistence.entity.ProjectFeeding;
import com.crm.persistence.entity.Team;
import com.crm.service.business.contract.RequestRespManager;
import com.crm.utils.service.RequestMetaData;

/**
 * @author manoj
 *
 */
@Transactional
@Stateless
public class RequestRespManageImpl implements RequestRespManager
{

	/**
	 * 
	 */
	@Inject
	private CrudService	crudService;
	
	@PersistenceContext(unitName="cr_master")
	private EntityManager em;

	RequestMetaData		requestMetaData;


	public RequestRespManageImpl()
	{
		// TODO Auto-generated constructor stub
	}

	@Override
	public CompanyProfile registerCompany(Map<String, String> requestMap)
	{
		CompanyProfile companyrequest = new CompanyProfile();
		companyrequest.setOrgnaizationName(requestMap.get("org_name").toString());
		companyrequest.setTaxID(requestMap.get("taxId").toString());
		companyrequest.setRegistrationNumber(requestMap.get("reg_number").toString());
		companyrequest.setPhone(requestMap.get("phone").toString());
		companyrequest.setEmail(requestMap.get("email").toString());
		companyrequest.setFax(requestMap.get("fax").toString());
		companyrequest.setAddressStreet1(requestMap.get("address_street1").toString());
		companyrequest.setAddressStreet2(requestMap.get("address_street2").toString());
		companyrequest.setCity(requestMap.get("city").toString());
		companyrequest.setState(requestMap.get("state").toString());
		companyrequest.setZipcode(requestMap.get("zipcode").toString());
		companyrequest.setCountry(requestMap.get("country").toString());
		companyrequest.setComment(requestMap.get("comment").toString());
		companyrequest.setStatus(requestMap.get("Status").toString());
		companyrequest.setAction(requestMap.get("Active").toString());
		companyrequest.setCreationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		companyrequest.setUpdationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		companyrequest.setCreatedBy(requestMap.get("createdBy").toString());
		companyrequest.setIpAddress(requestMap.get("ip_address").toString());
		companyrequest = crudService.create(companyrequest);
		return companyrequest;
	}

	@Override
	public Designation registerDesignation(Map<String, String> requestMap)
	{
		Designation designation = new Designation();
		designation.setDesignation(requestMap.get("designation").toString());
		designation.setStatus(requestMap.get("Status").toString());
		designation.setAction(requestMap.get("Active").toString());
		designation.setCreationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		designation.setUpdationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		designation.setCreatedBy(requestMap.get("createdBy").toString());
		designation.setIpAddress(requestMap.get("ip_address").toString());

		designation = crudService.create(designation);
		return designation;
	}

	@Override
	public Clientgroup registerClientGroup(Map<String, String> requestMap)
	{

		Clientgroup clientGroup = new Clientgroup();
		clientGroup.setName(requestMap.get("name").toString());
		clientGroup.setShortName(requestMap.get("short_name").toString());
		clientGroup.setDescription(requestMap.get("comment").toString());
		clientGroup.setStatus(requestMap.get("Status").toString());
		clientGroup.setAction(requestMap.get("Active").toString());
		clientGroup.setCreationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		clientGroup.setUpdationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		clientGroup.setCreatedBy(requestMap.get("createdBy").toString());
		clientGroup.setIpAddress(requestMap.get("ip_address").toString());

		clientGroup = crudService.create(clientGroup);
		return clientGroup;

	}

	@Override
	public OfficeLocation registerOfficeLocation(Map<String, String> requestMap)
	{
		OfficeLocation officeLocation = new OfficeLocation();

		officeLocation.setName(requestMap.get("name").toString());
		officeLocation.setContactNo(requestMap.get("phone").toString());
		officeLocation.setFax(requestMap.get("fax").toString());
		officeLocation.setAddressStreet1(requestMap.get("address_street1").toString());
		officeLocation.setAddressStreet2(requestMap.get("address_street2").toString());
		officeLocation.setCity(requestMap.get("city").toString());
		officeLocation.setState(requestMap.get("state").toString());
		officeLocation.setZipcode(requestMap.get("zipcode").toString());
		officeLocation.setCountry(requestMap.get("country").toString());
		officeLocation.setComment(requestMap.get("comment").toString());
		officeLocation.setStatus(requestMap.get("Status").toString());
		officeLocation.setAction(requestMap.get("Active").toString());
		officeLocation.setCreationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		officeLocation.setUpdationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		officeLocation.setCreatedBy(requestMap.get("createdBy").toString());
		officeLocation.setIpAddress(requestMap.get("ip_address").toString());

		officeLocation = crudService.create(officeLocation);
		return officeLocation;
	}

	@Override
	public Bank registerBank(Map<String, String> requestMap)
	{

		Bank bank = new Bank();

		bank.setName(requestMap.get("name").toString());
		bank.setContactNo(requestMap.get("phone").toString());
		bank.setEmail(requestMap.get("email").toString());
		bank.setFax(requestMap.get("fax").toString());
		bank.setAddressStreet1(requestMap.get("address_street1").toString());
		bank.setAddressStreet2(requestMap.get("address_street2").toString());
		bank.setCity(requestMap.get("city").toString());
		bank.setState(requestMap.get("state").toString());
		bank.setZipcode(requestMap.get("zipcode").toString());
		bank.setCountry(requestMap.get("country").toString());
		bank.setComment(requestMap.get("comment").toString());
		bank.setStatus(requestMap.get("Status").toString());
		bank.setAction(requestMap.get("Active").toString());
		bank.setCreationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		bank.setUpdationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		bank.setCreatedBy(requestMap.get("createdBy").toString());
		bank.setIpAddress(requestMap.get("ip_address").toString());

		bank = crudService.create(bank);
		return bank;
	}

	@Override
	public Client registerClient(Map<String, String> requestMap)
	{

		Client client = new Client();
		
		int id = Integer.parseInt(requestMap.get("clientGrpId").toString());
		
		Clientgroup clientGroup = em.getReference(Clientgroup.class, id);
		
		client.setName(requestMap.get("name").toString());

		client.setContactNo(requestMap.get("phone").toString());
		client.setEmail(requestMap.get("email").toString());
		client.setClientgroup(clientGroup);
		client.setStartDate(requestMap.get("startDate"));
		client.setAddressStreet1(requestMap.get("address_street1").toString());
		client.setAddressStreet2(requestMap.get("address_street2").toString());
		client.setCity(requestMap.get("city").toString());
		client.setState(requestMap.get("state").toString());
		client.setZipcode(requestMap.get("zipcode").toString());
		client.setCountry(requestMap.get("country").toString());
		client.setComment(requestMap.get("comment").toString());
		client.setStatus(requestMap.get("Status").toString());
		client.setAction(requestMap.get("Active").toString());
		client.setCreationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		client.setUpdationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		client.setCreatedBy(requestMap.get("createdBy").toString());
		client.setIpAddress(requestMap.get("ip_address").toString());

		client = crudService.create(client);
		return client;

	}

	@Override
	public Employee registerEmployee(Map<String, String> requestMap) {
		
		Employee employee = new Employee();
		
		employee.setName(requestMap.get("name").toString());
		employee.setDesignation(requestMap.get("designation").toString());
		employee.setWorkingLocId(Integer.parseInt(requestMap.get("workingLocId").toString()));
		employee.setContactNo(requestMap.get("phone").toString());
		employee.setEmail(requestMap.get("email").toString());
		employee.setAddressStreet1(requestMap.get("address_street1").toString());
		employee.setAddressStreet2(requestMap.get("address_street2").toString());
		employee.setCity(requestMap.get("city").toString());
		employee.setState(requestMap.get("state").toString());
		employee.setZipcode(requestMap.get("zipcode").toString());
		employee.setCountry(requestMap.get("country").toString());
		employee.setComment(requestMap.get("comment").toString());
		employee.setStatus(requestMap.get("Status").toString());
		employee.setAction(requestMap.get("Active").toString());
		employee.setCreationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		employee.setUpdationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		employee.setCreatedBy(requestMap.get("createdBy").toString());
		employee.setIpAddress(requestMap.get("ip_address").toString());
		
		employee = crudService.create(employee);
		
		return employee;
	
	}
	
	@Override
	public Team registerTeam(Map<String, String> requestMap) {
		
		Team team = new Team();

		int id = Integer.parseInt(requestMap.get("locationId").toString());
		
		OfficeLocation officeLocation = em.getReference(OfficeLocation.class, id);
		
		team.setName(requestMap.get("name").toString());
		team.setMember_empId(requestMap.get("member_empId").toString());
		team.setOfficeLocation(officeLocation);
		team.setOrgLevel(requestMap.get("org_level").toString());
		
		team.setStatus(requestMap.get("Status").toString());
		team.setAction(requestMap.get("Active").toString());
		team.setCreationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		team.setUpdationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		team.setCreatedBy(requestMap.get("createdBy").toString());
		team.setIpAddress(requestMap.get("ip_address").toString());
		
		team = crudService.create(team);
		
		return team;
	
	}
	
	@Override
	public ProjectFeeding registerProjectFeed(Map<String, String> requestMap) {
		
		ProjectFeeding projectFeeding = new ProjectFeeding();

		projectFeeding.setClientName(requestMap.get("clientName").toString());
		projectFeeding.setPositiveBanks(requestMap.get("positiveBanks").toString());
		projectFeeding.setNegativeBanks(requestMap.get("negativeBanks").toString());
		projectFeeding.setReasonForNegativeBanks(requestMap.get("reason").toString());
		projectFeeding.setStartDate(requestMap.get("startDate").toString());
		projectFeeding.setStatus(requestMap.get("Status").toString());
		projectFeeding.setAction(requestMap.get("Active").toString());
		projectFeeding.setCreationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		projectFeeding.setUpdationDate(new java.sql.Timestamp(new java.util.Date().getTime()));
		projectFeeding.setCreatedBy(requestMap.get("createdBy").toString());
		projectFeeding.setIpAddress(requestMap.get("ip_address").toString());
		
		projectFeeding = crudService.create(projectFeeding);
		
		// TODO Auto-generated method stub
		return projectFeeding;
	}
	
	@Override
	public List<Map<String, String>> getCompanyMaster()
	{
		List<Map<String, String>> companyMasters = new ArrayList<>();

		List<CompanyProfile> registeredCompnayList = crudService
				.findWithNamedQuery("CompanyProfile.findAllByStatus",QueryParameter.with("status", "active").parameters());
		for (CompanyProfile companyProfile : registeredCompnayList)
		{
			companyMasters.add(this.setCompanyAttr(companyProfile));
		}

		return companyMasters;
	}

	@Override
	public List<Map<String, String>> getOfficeLocMaster() {
		
		List<Map<String,String>> officeLocMasters = new ArrayList<>();
		
		List<OfficeLocation> officeLocList = crudService.findWithNamedQuery("OfficeLocation.findAllByActive",QueryParameter.with("active", "active").parameters());
		
		for(OfficeLocation officeLoc : officeLocList)
		{
			officeLocMasters.add(this.setOfficeAttr(officeLoc));
		}
	
		return officeLocMasters;
	}
	
	@Override
	public List<Map<String, String>> getDesignationMaster() {
		
		List<Map<String,String>> designationMasters = new ArrayList<>();
		
		List<Designation> designationList =  crudService.findWithNamedQuery("Designation.findAllByStatus",QueryParameter.with("active", "active").parameters());
		
		for(Designation designation : designationList)
		{
			designationMasters.add(this.setDesignationAttr(designation));
		}
		
		return designationMasters;
	}
	

	@Override
	public List<Map<String, String>> getClientGroupMaster() {
		
		List<Map<String,String>> clientGrpMasters = new ArrayList<>();
		
		List<Clientgroup> clientGrpList =  crudService.findWithNamedQuery("Clientgroup.findAllByStatus",QueryParameter.with("active", "active").parameters());
		
		for(Clientgroup clientGrp : clientGrpList)
		{
			clientGrpMasters.add(this.setClientGroupAttr(clientGrp));
		}
		
		return clientGrpMasters;
	}

	@Override
	public List<Map<String, String>> getBanksMaster() {
		
		List<Map<String, String>> bankMasters = new ArrayList<>();
		
		List<Bank> bankList = crudService.findWithNamedQuery("Bank.findByStatus",QueryParameter.with("active", "active").parameters());
		
		for(Bank bank : bankList)
		{
			bankMasters.add(this.setBanksAttr(bank));
		}
		return bankMasters;
	}

	@Override
	public List<Map<String, String>> getClientsMaster() {
		
		List<Map<String, String>> clientsMasters = new ArrayList<>();
		
		List<Client> clientList = crudService.findWithNamedQuery("Client.findAllByStatus",QueryParameter.with("active", "active").parameters());
		
		for(Client client : clientList)
		{
			clientsMasters.add(this.setClientsAttr(client));
		}
		return clientsMasters;
	}

	@Override
	public List<Map<String, String>> getEmployeeMaster() {

		List<Map<String, String>> employeeMasters = new ArrayList<>();
		
		List<Employee> employeeList = crudService.findWithNamedQuery("Employee.findAllByStatus",QueryParameter.with("active", "active").parameters());
		
		for(Employee employee : employeeList)
		{
			employeeMasters.add(this.setEmployeeAttr(employee));
		}
		return employeeMasters;
		
	}

	@Override
	public List<Map<String, String>> getTeamMaster() {
		
		List<Map<String, String>> teamMasters = new ArrayList<>();
		
		List<Team> teamList = crudService.findWithNamedQuery("Team.findAllByStatus",QueryParameter.with("active", "active").parameters());
		
		for(Team team : teamList)
		{
			teamMasters.add(this.setTeamAttr(team));
		}
		return teamMasters;
		
	}
	
	@Override
	public List<Map<String, String>> getProjectFeedMaster() {
		
		List<Map<String, String>> projectFeedMasters = new ArrayList<>();
		
		List<ProjectFeeding> projectFeedList = crudService.findWithNamedQuery("ProjectFeeding.findAllByStatus",QueryParameter.with("active", "active").parameters());
		
		for(ProjectFeeding projectFeed : projectFeedList)
		{
			projectFeedMasters.add(this.setProjectFeedAttr(projectFeed));
		}
		return projectFeedMasters;
		
	}
	
	//	public <T> T setAttrToMap(T t){
	//		Map<String, String> masterMetaData = new HashMap<>();
	//		List<String> listData = new ArrayList<>();
	//		masterMetaData.put(requestMetaData, )
	//		
	//		return t;
	//		
	//	}

	public Map<String, String> setCompanyAttr(CompanyProfile companyProfile)
	{
		Map<String, String> companyMaster = new HashMap<>();
		if (!companyProfile.toString().isEmpty() && companyProfile.toString().length() > 0)
		{
			companyMaster.put(requestMetaData.Id, Integer.toString(companyProfile.getId()));
			companyMaster.put(requestMetaData.Name, companyProfile.getOrgnaizationName());
			companyMaster.put("Address",
					companyProfile.getAddressStreet1() + "," + companyProfile.getAddressStreet2()
							+ "," + companyProfile.getCity() + "," + companyProfile.getCountry());
			companyMaster.put(requestMetaData.Action, companyProfile.getAction());
			companyMaster.put(requestMetaData.CreationDate,
					companyProfile.getCreationDate().toString());
			companyMaster.put(requestMetaData.Status, companyProfile.getStatus());
		}

		return companyMaster;
	}

	public Map<String, String> setOfficeAttr(OfficeLocation officeLoc)
	{
		Map<String,String> officeMaster = new HashMap<>();
		
		if(!officeLoc.toString().isEmpty() && officeLoc.toString().length()>0 )
		{
			officeMaster.put(requestMetaData.Id, Integer.toString(officeLoc.getId()));
			officeMaster.put(requestMetaData.Name, officeLoc.getName());
			officeMaster.put("Address", officeLoc.getAddressStreet1()+ ","+ officeLoc.getAddressStreet2()+"," +officeLoc.getCity()+","+officeLoc.getCountry());
			officeMaster.put(requestMetaData.Action, officeLoc.getAction());
			officeMaster.put(requestMetaData.CreationDate, officeLoc.getCreationDate().toString());
			officeMaster.put(requestMetaData.Status, officeLoc.getStatus());
			
		}
		return officeMaster;
	}
	
	public Map<String,String> setDesignationAttr(Designation designation)
	{
		Map<String,String> designationMaster = new HashMap<>();
		
		if(!designation.toString().isEmpty() && designation.toString().length()>0)
		{
			designationMaster.put(requestMetaData.Id, Integer.toString(designation.getId()));
			designationMaster.put(requestMetaData.Name, designation.getDesignation());
			designationMaster.put(requestMetaData.Action, designation.getAction());
			designationMaster.put(requestMetaData.CreationDate, designation.getCreationDate().toString());
			designationMaster.put(requestMetaData.Status, designation.getStatus());
		}
		return designationMaster;
	}

	public Map<String, String> setClientGroupAttr(Clientgroup clientgrp)
	{
		Map<String,String> clientGrpMaster = new HashMap<>();
		
		if(!clientGrpMaster.toString().isEmpty() && clientGrpMaster.toString().length()>0)
		{
			clientGrpMaster.put(requestMetaData.Id, Integer.toString(clientgrp.getId()));
			clientGrpMaster.put(requestMetaData.Name, clientgrp.getName());
			clientGrpMaster.put("Description", clientgrp.getDescription());
			clientGrpMaster.put(requestMetaData.Action, clientgrp.getAction());
			clientGrpMaster.put(requestMetaData.CreationDate, clientgrp.getCreationDate().toString());
			clientGrpMaster.put(requestMetaData.Status, clientgrp.getStatus());
		}
		return clientGrpMaster;
	}

	public Map<String, String> setBanksAttr(Bank bank)
	{
		Map<String,String> bankMaster = new HashMap<>();
		
		if(!bankMaster.toString().isEmpty() && bankMaster.toString().length()>0)
		{
			bankMaster.put(requestMetaData.Id, Integer.toString(bank.getId()));
			bankMaster.put(requestMetaData.Name, bank.getName());
			bankMaster.put("Address",
					bank.getAddressStreet1() + "," + bank.getAddressStreet2()
							+ "," + bank.getCity() + "," + bank.getCountry());
			bankMaster.put(requestMetaData.Action, bank.getAction());
			bankMaster.put(requestMetaData.CreationDate, bank.getCreationDate().toString());
			bankMaster.put(requestMetaData.Status, bank.getStatus());
		}
		return bankMaster;
	}

	public Map<String, String> setClientsAttr(Client client)
	{
		Map<String,String> clientMaster = new HashMap<>();
		
		if(!clientMaster.toString().isEmpty() && clientMaster.toString().length()>0)
		{
			clientMaster.put(requestMetaData.Id, Integer.toString(client.getId()));
			clientMaster.put(requestMetaData.Name, client.getName());
			clientMaster.put("Address",
					client.getAddressStreet1() + "," + client.getAddressStreet2()
							+ "," + client.getCity() + "," + client.getCountry());
			clientMaster.put(requestMetaData.Action, client.getAction());
			clientMaster.put(requestMetaData.CreationDate, client.getCreationDate().toString());
			clientMaster.put(requestMetaData.Status, client.getStatus());
		}
		return clientMaster;
	}
	
	public Map<String, String> setEmployeeAttr(Employee employee)
	{
		Map<String,String> employeeMaster = new HashMap<>();
		
		if(!employeeMaster.toString().isEmpty() && employeeMaster.toString().length()>0)
		{
			employeeMaster.put(requestMetaData.Id, Integer.toString(employee.getId()));
			employeeMaster.put(requestMetaData.Name, employee.getName());
			employeeMaster.put("Address",
					employee.getAddressStreet1() + "," + employee.getAddressStreet2()
							+ "," + employee.getCity() + "," + employee.getCountry());
			employeeMaster.put(requestMetaData.Action, employee.getAction());
			employeeMaster.put(requestMetaData.CreationDate, employee.getCreationDate().toString());
			employeeMaster.put(requestMetaData.Status, employee.getStatus());
		}
		return employeeMaster;
	}

	public Map<String, String> setTeamAttr(Team team)
	{
		Map<String,String> teamMaster = new HashMap<>();
		
		if(!teamMaster.toString().isEmpty() && teamMaster.toString().length()>0)
		{
			teamMaster.put(requestMetaData.Id, Integer.toString(team.getId()));
			teamMaster.put(requestMetaData.Name, team.getName());
			teamMaster.put("Orgnization Level",
					team.getOrgLevel());
			teamMaster.put(requestMetaData.Action, team.getAction());
			teamMaster.put(requestMetaData.CreationDate, team.getCreationDate().toString());
			teamMaster.put(requestMetaData.Status, team.getStatus());
		}
		return teamMaster;
	}
	
	public Map<String, String> setProjectFeedAttr(ProjectFeeding projectFeed)
	{
		Map<String,String> projectFeedMaster = new HashMap<>();
		
		if(!projectFeedMaster.toString().isEmpty() && projectFeedMaster.toString().length()>0)
		{
			projectFeedMaster.put(requestMetaData.Id, Integer.toString(projectFeed.getId()));
			projectFeedMaster.put(requestMetaData.Name, projectFeed.getClientName());
			projectFeedMaster.put(requestMetaData.Status,projectFeed.getStatus());
			projectFeedMaster.put(requestMetaData.Action, projectFeed.getAction());
			projectFeedMaster.put(requestMetaData.CreationDate, projectFeed.getCreationDate().toString());
			projectFeedMaster.put(requestMetaData.Status, projectFeed.getStatus());
		}
		return projectFeedMaster;
	}

	@Override
	public CompanyProfile deleteCompany(Map<String, String> requestMap) {
		
		CompanyProfile companyProfile = new CompanyProfile();
		
		int id = Integer.parseInt(requestMap.get("id").toString());
		
		List<CompanyProfile> updatedlist = crudService.findWithNamedQuery("CompanyProfile.findAllByID",QueryParameter.with("id", id).parameters());
		
		for (CompanyProfile update : updatedlist) {
			update.setStatus("Inactive");
			companyProfile = crudService.update(update);
		}

		return companyProfile;
	}

	@Override
	public OfficeLocation deleteOfficeLocation(Map<String, String> requestMap) {
		
		OfficeLocation officeLocation = new OfficeLocation();
		
		int id = Integer.parseInt(requestMap.get("id").toString());
		
		List<OfficeLocation> officeLocationlist = crudService.findWithNamedQuery("OfficeLocation.findAllByID",QueryParameter.with("id", id).parameters());
		
		for (OfficeLocation update : officeLocationlist) {
			update.setStatus("Inactive");
			officeLocation = crudService.update(update);
		}
		return officeLocation;
	}

	@Override
	public Designation deleteDesignation(Map<String, String> requestMap) {
		
		Designation designation = new Designation();
		
		int id = Integer.parseInt(requestMap.get("id").toString());
		
		List<Designation> designationList = crudService.findWithNamedQuery("Designation.findAllByID",QueryParameter.with("id", id).parameters());
		
		for (Designation update : designationList) {
			update.setStatus("Inactive");
			designation = crudService.update(update);
		}
		return designation;
	}

	@Override
	public Clientgroup deleteClientGroup(Map<String, String> requestMap) {
		
		Clientgroup clientGroup = new Clientgroup();
		
		int id = Integer.parseInt(requestMap.get("id").toString());
		
		List<Clientgroup> clientGroupList = crudService.findWithNamedQuery("Clientgroup.findByID",QueryParameter.with("id", id).parameters());
		
		for (Clientgroup update : clientGroupList) {
			update.setStatus("Inactive");
			clientGroup = crudService.update(update);
		}
		return clientGroup;

	}

	@Override
	public Bank deleteBank(Map<String, String> requestMap) {
		
		Bank bank = new Bank();
		
		int id = Integer.parseInt(requestMap.get("id").toString());
		
		List<Bank> bankList = crudService.findWithNamedQuery("Bank.findByID",QueryParameter.with("id", id).parameters());
		
		for (Bank update : bankList) {
			update.setStatus("Inactive");
			bank = crudService.update(update);
		}
		return bank;
		
	}

	@Override
	public Client deleteClient(Map<String, String> requestMap) {
	
		Client client = new Client();
		
		int id = Integer.parseInt(requestMap.get("id").toString());
		
		List<Client> clientList = crudService.findWithNamedQuery("Client.findByID",QueryParameter.with("id", id).parameters());
		
		for (Client update : clientList) {
			update.setStatus("Inactive");
			client = crudService.update(update);
		}
		return client;
	}

	@Override
	public Employee deleteEmployee(Map<String, String> requestMap) {
		
		Employee employee = new Employee();
		
		int id = Integer.parseInt(requestMap.get("id").toString());
		
		List<Employee> employeeList = crudService.findWithNamedQuery("Employee.findByID",QueryParameter.with("id", id).parameters());
		
		for (Employee update : employeeList) {
			update.setStatus("Inactive");
			employee = crudService.update(update);
		}
		return employee;
		
	}

	@Override
	public Team deleteTeam(Map<String, String> requestMap) {
		
		Team team = new Team();
		
		int id = Integer.parseInt(requestMap.get("id").toString());
		
		List<Team> teamList = crudService.findWithNamedQuery("Team.findByID",QueryParameter.with("id", id).parameters());
		
		for (Team update : teamList) {
			update.setStatus("Inactive");
			team = crudService.update(update);
		}
		return team;
		
	}

	@Override
	public ProjectFeeding deleteProjectFeed(Map<String, String> requestMap) {
		
		ProjectFeeding projectFeeding = new ProjectFeeding();
		
		int id = Integer.parseInt(requestMap.get("id").toString());
		
		List<ProjectFeeding> projectList = crudService.findWithNamedQuery("ProjectFeeding.findAllByID",QueryParameter.with("id", id).parameters());
		
		for (ProjectFeeding update : projectList) {
			update.setStatus("Inactive");
			projectFeeding = crudService.update(update);
		}
		return projectFeeding;
		
	}


	
}
