/**
 * 
 */
package com.crm.service.business.contract;

import javax.ejb.Local;

import com.crm.persistence.entity.Employee;

/**
 * @author manoj
 *
 */
@Local
public interface EAuthService
{
	public boolean isAuthenticated(String email);

	public Employee setEmpData(String email);

	
}
