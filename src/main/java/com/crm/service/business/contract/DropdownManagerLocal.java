package com.crm.service.business.contract;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface DropdownManagerLocal
{

	List<String> officeLocation();

	List<String> getAllDesignations();

	public Map<String, String> getClientGroup();
	
	public Map<String, String> getOfficeLoc();
	
	public Map<String, String> getEmployeeData();
	
	public Map<String, String> getBankData();
	
	public Map<String, String> getClientData();

}
