/**
 * 
 */
package com.crm.service.business.contract;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.crm.persistence.entity.Bank;
import com.crm.persistence.entity.Client;
import com.crm.persistence.entity.Clientgroup;
import com.crm.persistence.entity.CompanyProfile;
import com.crm.persistence.entity.Designation;
import com.crm.persistence.entity.Employee;
import com.crm.persistence.entity.OfficeLocation;
import com.crm.persistence.entity.ProjectFeeding;
import com.crm.persistence.entity.Team;

/**
 * @author manoj
 *
 */
@Local
public interface RequestRespManager
{

	public CompanyProfile registerCompany (Map<String, String> requestMap);
	
	public CompanyProfile deleteCompany(Map<String, String> requestMap);
	
	public Designation registerDesignation (Map<String, String> requestMap);
	
	public Designation deleteDesignation (Map<String, String> requestMap);
	
	public Clientgroup registerClientGroup (Map<String, String> requestMap);
	
	public Clientgroup deleteClientGroup (Map<String, String> requestMap);
	
	public OfficeLocation registerOfficeLocation (Map<String, String> requestMap);
	
	public OfficeLocation deleteOfficeLocation (Map<String, String> requestMap);
	
	public Bank registerBank (Map<String, String> requestMap);
	
	public Bank deleteBank (Map<String, String> requestMap);
	
	public Client registerClient (Map<String, String> requestMap);
	
	public Client deleteClient (Map<String, String> requestMap);
	
	public Employee registerEmployee (Map<String, String> requestMap);
	
	public Employee deleteEmployee (Map<String, String> requestMap);
	
	public Team registerTeam (Map<String, String> requestMap);
	
	public Team deleteTeam (Map<String, String> requestMap);
	
	public ProjectFeeding registerProjectFeed (Map<String, String> requestMap); 
	
	public ProjectFeeding deleteProjectFeed (Map<String, String> requestMap);
	
	public List<Map<String, String>> getCompanyMaster();
	
	public List<Map<String,String>> getOfficeLocMaster();
	
	public List<Map<String,String>> getDesignationMaster();
	
	public List<Map<String,String>> getClientGroupMaster();
	
	public List<Map<String,String>> getBanksMaster();
	
	public List<Map<String,String>> getClientsMaster();
	
	public List<Map<String,String>> getEmployeeMaster();
	
	public List<Map<String,String>> getTeamMaster();
	
	public List<Map<String,String>> getProjectFeedMaster();
	
	
	
	
}
