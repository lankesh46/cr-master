/**
 * 
 */
package com.crm.utils.service;

/**
 * @author manoj
 *
 */
public interface RequestMetaData
{

	String	Id				= "ID";
	String	Name			= "Name";
	String	Status			= "Status";
	String	Action			= "Action";
	String	CreationDate	= "Create Date";
	String	UpdationDate	= "Update Date";
	String	CreatedBy		= "CreatedBy";
	String	Ip				= "IP Address";

}
