/**
 * 
 */
package com.crm.utils.service;

/**
 * @author manoj
 *
 */
public interface AlertMessages
{
	String requestSent = " Your request has been submitted successfully ";
	String requestDelete = "Successfully deleted";
}
