/**
 * 
 */
package com.crm.views;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.crm.service.business.contract.DropdownManagerLocal;
import com.crm.service.business.impl.DropdownManager;

/**
 * @author manoj
 *
 */
@Named("dropdown")
@RequestScoped
public class DropdownsBean
{

	@Inject
	DropdownManager		dropdownManager;
	
	@Inject
	DropdownManagerLocal dropdownManagerLocal;

	Map<String, String>	clientGroup;

	Map<String, String>	officeLoc;

	Map<String, String> employeedata;
	
	Map<String, String> bankdata;	
	
	Map<String, String> clientData;
	
	public DropdownsBean()
	{
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void init()
	{
		clientGroup = dropdownManager.getClientGroup();
		officeLoc = dropdownManagerLocal.getOfficeLoc();
		employeedata = dropdownManagerLocal.getEmployeeData();
		bankdata = dropdownManagerLocal.getBankData();
		clientData = dropdownManagerLocal.getClientData();
		System.out.println("");
	}

	public Map<String, String> getClientGroup()
	{
		return clientGroup;
	}

	public void setClientGroup(Map<String, String> clientGroup)
	{
		this.clientGroup = clientGroup;
	}

	public Map<String, String> getOfficeLoc()
	{
		return officeLoc;
	}

	public void setOfficeLoc(Map<String, String> officeLoc)
	{
		this.officeLoc = officeLoc;
	}

	public Map<String, String> getEmployeedata() {
		return employeedata;
	}

	public void setEmployeedata(Map<String, String> employeedata) {
		this.employeedata = employeedata;
	}

	public Map<String, String> getBankdata() {
		return bankdata;
	}

	public void setBankdata(Map<String, String> bankdata) {
		this.bankdata = bankdata;
	}

	public Map<String, String> getClientData() {
		return clientData;
	}

	public void setClientData(Map<String, String> clientData) {
		this.clientData = clientData;
	}

	
}
