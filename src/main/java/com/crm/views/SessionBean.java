package com.crm.views;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.Init;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.crm.persistence.entity.Employee;

@Named("usersession")
@SessionScoped
public class SessionBean implements Serializable
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	private Employee			employee;

	public SessionBean()
	{

	}
	
	@PostConstruct
	public void init(){
		
	}

	public Employee getEmployee()
	{
		return employee;
	}

	public void setEmployee(Employee employee)
	{
		this.employee = employee;
	}

}
