package com.crm.views;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.crm.service.business.contract.RequestRespManager;

/**
 * @author manoj
 *
 */
/**
 * @author Manoj
 *
 */
@Named("requestBean")
@RequestScoped
public class MasterReqBean implements Serializable
{

	/**
	 * 
	 */
	private static final long			serialVersionUID	= 1L;

	private List<Map<String, String>>	masterCompany;
	
	private List<Map<String, String>>   masterOfficeLoc;
	
	private List<Map<String, String>>	masterDesignation;
	
	private List<Map<String, String>>	masterClientGrp;
	
	private List<Map<String, String>>   masterBanks;
	
	private List<Map<String, String>>   masterClients;
	
	private List<Map<String, String>>   masterEmployee;
	
	private List<Map<String, String>>   masterTeam;
	
	private List<Map<String, String>>   masterProjectFeed;
	
	
	@Inject
	RequestRespManager					requestRespManager;

	public MasterReqBean()
	{
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	private void init()
	{
		masterCompany = requestRespManager.getCompanyMaster();
		masterOfficeLoc = requestRespManager.getOfficeLocMaster();
		masterDesignation = requestRespManager.getDesignationMaster();
		masterClientGrp = requestRespManager.getClientGroupMaster();
		masterBanks = requestRespManager.getBanksMaster();
		masterClients = requestRespManager.getClientsMaster();
		masterEmployee = requestRespManager.getEmployeeMaster();
		masterTeam = requestRespManager.getTeamMaster();
		masterProjectFeed = requestRespManager.getProjectFeedMaster();
	}

	/**
	 * @return the masterCompany
	 */
	public List<Map<String, String>> getMasterCompany()
	{
		return masterCompany;
	}

	/**
	 * @param masterCompany
	 *            the masterCompany to set
	 */
	public void setMasterCompany(List<Map<String, String>> masterCompany)
	{
		this.masterCompany = masterCompany;
	}

	public List<Map<String, String>> getMasterOfficeLoc() {
		return masterOfficeLoc;
	}

	public void setMasterOfficeLoc(List<Map<String, String>> masterOfficeLoc) {
		this.masterOfficeLoc = masterOfficeLoc;
	}

	public List<Map<String, String>> getMasterDesignation() {
		return masterDesignation;
	}

	public void setMasterDesignation(List<Map<String, String>> masterDesignation) {
		this.masterDesignation = masterDesignation;
	}

	public List<Map<String, String>> getMasterClientGrp() {
		return masterClientGrp;
	}

	public void setMasterClientGrp(List<Map<String, String>> masterClientGrp) {
		this.masterClientGrp = masterClientGrp;
	}

	public List<Map<String, String>> getMasterBanks() {
		return masterBanks;
	}

	public void setMasterBanks(List<Map<String, String>> masterBanks) {
		this.masterBanks = masterBanks;
	}

	public List<Map<String, String>> getMasterClients() {
		return masterClients;
	}

	public void setMasterClients(List<Map<String, String>> masterClients) {
		this.masterClients = masterClients;
	}

	public List<Map<String, String>> getMasterEmployee() {
		return masterEmployee;
	}

	public void setMasterEmployee(List<Map<String, String>> masterEmployee) {
		this.masterEmployee = masterEmployee;
	}

	public List<Map<String, String>> getMasterTeam() {
		return masterTeam;
	}

	public void setMasterTeam(List<Map<String, String>> masterTeam) {
		this.masterTeam = masterTeam;
	}

	public List<Map<String, String>> getMasterProjectFeed() {
		return masterProjectFeed;
	}

	public void setMasterProjectFeed(List<Map<String, String>> masterProjectFeed) {
		this.masterProjectFeed = masterProjectFeed;
	}
	
	
}
