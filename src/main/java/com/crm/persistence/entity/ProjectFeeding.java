package com.crm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the project_feeding database table.
 * 
 */
@Entity
@Table(name="project_feeding")

@NamedQueries({
	@NamedQuery(name="ProjectFeeding.findAll", query="SELECT p FROM ProjectFeeding p"),
	@NamedQuery(name="ProjectFeeding.findAllByID", query="SELECT p FROM ProjectFeeding p WHERE p.id =:id"),
	@NamedQuery(name="ProjectFeeding.findAllByStatus", query="SELECT p FROM ProjectFeeding p WHERE p.status =:active"),
})
public class ProjectFeeding implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String action;

	private String clientName;

	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Column(name="ip_address")
	private String ipAddress;

	private String negativeBanks;

	private String positiveBanks;

	private String reasonForNegativeBanks;

	private String startDate;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updationDate;

	public ProjectFeeding() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getClientName() {
		return this.clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getNegativeBanks() {
		return this.negativeBanks;
	}

	public void setNegativeBanks(String negativeBanks) {
		this.negativeBanks = negativeBanks;
	}

	public String getPositiveBanks() {
		return this.positiveBanks;
	}

	public void setPositiveBanks(String positiveBanks) {
		this.positiveBanks = positiveBanks;
	}

	public String getReasonForNegativeBanks() {
		return this.reasonForNegativeBanks;
	}

	public void setReasonForNegativeBanks(String reasonForNegativeBanks) {
		this.reasonForNegativeBanks = reasonForNegativeBanks;
	}

	public String getStartDate() {
		return this.startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdationDate() {
		return this.updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}

}