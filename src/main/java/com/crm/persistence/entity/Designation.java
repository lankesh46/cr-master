package com.crm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the designations database table.
 * 
 */
@Entity
@Table(name="designations")
@NamedQueries({
	@NamedQuery(name="Designation.findAll", query="SELECT d FROM Designation d"),
	@NamedQuery(name="Designation.findAllByID", query="SELECT d FROM Designation d WHERE d.id=:id"),
	@NamedQuery(name="Designation.findAllByStatus", query="SELECT d FROM Designation d WHERE d.status=:active")
})
public class Designation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String action;

	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	private String designation;

	@Column(name="ip_address")
	private String ipAddress;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updationDate;

	public Designation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdationDate() {
		return this.updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}

}