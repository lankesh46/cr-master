package com.crm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the clientgroup database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Clientgroup.findAll", query="SELECT c FROM Clientgroup c"),
	@NamedQuery(name="Clientgroup.findAllByStatus", query="SELECT c FROM Clientgroup c WHERE c.status =:active"),
	@NamedQuery(name="Clientgroup.findByID", query="SELECT c FROM Clientgroup c WHERE c.id = :id"),
})
public class Clientgroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String action;

	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	private String description;

	@Column(name="ip_address")
	private String ipAddress;

	private String name;

	private String shortName;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updationDate;

	//bi-directional many-to-one association to Client
	@OneToMany(mappedBy="clientgroup")
	private List<Client> clients;

	public Clientgroup() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdationDate() {
		return this.updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}

	public List<Client> getClients() {
		return this.clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public Client addClient(Client client) {
		getClients().add(client);
		client.setClientgroup(this);

		return client;
	}

	public Client removeClient(Client client) {
		getClients().remove(client);
		client.setClientgroup(null);

		return client;
	}

}