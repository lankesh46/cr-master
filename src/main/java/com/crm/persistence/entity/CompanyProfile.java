package com.crm.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the company_profile database table.
 * 
 */
@Entity
@Table(name="company_profile")
@NamedQueries({
	@NamedQuery(name="CompanyProfile.findAll", query="SELECT c FROM CompanyProfile c"),
	@NamedQuery(name="CompanyProfile.findAllByID", query="SELECT c FROM CompanyProfile c WHERE c.id=:id"),
	@NamedQuery(name="CompanyProfile.findAllByStatus", query="SELECT c FROM CompanyProfile c WHERE c.status=:status")
})

public class CompanyProfile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String action;

	@Column(name="address_street1")
	private String addressStreet1;

	@Column(name="address_street2")
	private String addressStreet2;

	private String city;

	private String comment;

	private String country;

	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	private String email;

	private String fax;

	@Column(name="ip_address")
	private String ipAddress;

	@Column(name="orgnaization_name")
	private String orgnaizationName;

	private String phone;

	@Column(name="registration_number")
	private String registrationNumber;

	private String state;

	private String status;

	private String taxID;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updationDate;

	private String zipcode;

	public CompanyProfile() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAddressStreet1() {
		return this.addressStreet1;
	}

	public void setAddressStreet1(String addressStreet1) {
		this.addressStreet1 = addressStreet1;
	}

	public String getAddressStreet2() {
		return this.addressStreet2;
	}

	public void setAddressStreet2(String addressStreet2) {
		this.addressStreet2 = addressStreet2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getOrgnaizationName() {
		return this.orgnaizationName;
	}

	public void setOrgnaizationName(String orgnaizationName) {
		this.orgnaizationName = orgnaizationName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRegistrationNumber() {
		return this.registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTaxID() {
		return this.taxID;
	}

	public void setTaxID(String taxID) {
		this.taxID = taxID;
	}

	public Date getUpdationDate() {
		return this.updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}