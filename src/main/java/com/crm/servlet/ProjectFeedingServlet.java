package com.crm.servlet;

import java.io.IOException;
import java.sql.DriverManager;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Connection;
import java.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.crm.utils.service.AlertMessages;

import com.crm.service.business.contract.RequestRespManager;

@WebServlet("/ProjectFeedingServlet")
@MultipartConfig
public class ProjectFeedingServlet extends HttpServlet
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	private Logger				log					= LoggerFactory.getLogger(getClass());

	@Inject
	private RequestRespManager	requestManager;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Map<String, String> reqMap = new HashMap<>();
		String action = request.getParameter("action").toString();
		log.debug("Action for companny profile is generated");
		
	
		
		if (action.equals("GENERATED"))
		{
		
			String[] clientName = request.getParameterValues("clientName");
			String[] positiveBanks = request.getParameterValues("positiveBanks");
			String[] negativeBanks = request.getParameterValues("negativeBanks");
			
			String res_clientName = "";
			String res_positiveBanks = "";
			String res_negativeBanks = "";
			
			for(int i = 0; i < clientName.length; i++)
			{
				res_clientName = res_clientName + clientName[i]+",";
			}
			
			for(int j = 0; j < positiveBanks.length; j++)
			{
				res_positiveBanks = res_positiveBanks + positiveBanks[j]+",";
			}
			
			for(int k = 0; k < negativeBanks.length; k++)
			{
				res_negativeBanks = res_negativeBanks + negativeBanks[k]+",";
			}
			
			String clientName_concat = res_clientName.concat("|");
			String positiveBanks_concat = res_positiveBanks.concat("|");
			String negativeBanks_concat = res_negativeBanks.concat("|");
			
			String clientName_values = clientName_concat.replace(",|", "");
			String positiveBanks_values = positiveBanks_concat.replace(",|", "");
			String negativeBanks_values = negativeBanks_concat.replace(",|", "");
			
			
			String reason = request.getParameter("reason");
			String startDate = request.getParameter("startDate");
			
			reqMap.put("clientName", clientName_values);
			reqMap.put("positiveBanks", positiveBanks_values);
			reqMap.put("negativeBanks", negativeBanks_values);
			reqMap.put("reason", reason);
			reqMap.put("startDate", startDate);
			reqMap.put("Status", "active");
			reqMap.put("Active", "1");
			reqMap.put("createdBy", "Admin");
			reqMap.put("ip_address", request.getRemoteAddr());

			requestManager.registerProjectFeed(reqMap);

			HttpSession session = request.getSession(false);
			session.setAttribute("isPRG", true);
			response.sendRedirect("PRGServlet?status=success&url=projectFeeding.jsp&alertMessage="
					+ AlertMessages.requestSent);

		}
		
		else if(action.equals("delete"))
		{
			
			log.debug("Delete Action for ProjectFeeding is generated");
		
			String id = request.getParameter("id");
			
			reqMap.put("id", id);
			
			requestManager.deleteProjectFeed(reqMap);
			
			HttpSession session = request.getSession(false);
			session.setAttribute("isPRG", true);
			
			response.sendRedirect("PRGServlet?status=success&url=projectFeeding.jsp&alertMessage="
					+ AlertMessages.requestDelete);
			
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		doGet(request, response);
	}

}
