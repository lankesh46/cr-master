package com.crm.servlet;

import java.io.IOException;
import java.sql.DriverManager;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.PreparedStatement;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.sql.Connection;
import java.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.crm.service.business.contract.RequestRespManager;
import com.crm.utils.service.AlertMessages;

@WebServlet("/OfficeLocationsServlet")
@MultipartConfig
public class OfficeLocationsServlet extends HttpServlet
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	private Logger				log					= LoggerFactory.getLogger(getClass());

	@Inject
	private RequestRespManager	requestManager;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Map<String, String> reqMap = new HashMap<>();
		String action = request.getParameter("action").toString();
		log.debug("Action for companny profile is generated");
				String name = request.getParameter("name");
				String phone = request.getParameter("contactNo");
				String fax = request.getParameter("fax");
				String address_street1 = request.getParameter("address_street1");
				String address_street2 = request.getParameter("address_street2");
				String city = request.getParameter("city");
				String state = request.getParameter("state");
				String zipcode = request.getParameter("zipcode");
				String country = request.getParameter("country");
				String comment = request.getParameter("comment");
		//		String status = request.getParameter("status");
		//		String active = request.getParameter("action");
		//
		//		String createdBy = request.getParameter("createdBy");
		//		String ip_address = "";
		if (action.equals("GENERATED"))
		{	
			
			reqMap.put("name", name);
			reqMap.put("phone", phone);
			reqMap.put("fax", fax);
			reqMap.put("address_street1", address_street1);
			reqMap.put("address_street2", address_street2);
			reqMap.put("city", city);
			reqMap.put("state", state);
			reqMap.put("zipcode", zipcode);
			reqMap.put("country", country);
			reqMap.put("comment", comment);
			reqMap.put("Status", "active");
			reqMap.put("Active", "1");
			reqMap.put("createdBy","Admin");
			reqMap.put("ip_address",request.getRemoteAddr());

			requestManager.registerOfficeLocation(reqMap);
			
			HttpSession session = request.getSession(false);
			session.setAttribute("isPRG", true);
			response.sendRedirect("PRGServlet?status=success&url=officeLocation.jsp&alertMessage="
					+ AlertMessages.requestSent);
		}

		else if(action.equals("delete"))
		{
			
			log.debug("Delete Action for OfficeLocation is generated");
		
			String id = request.getParameter("id");
			
			reqMap.put("id", id);
			
			requestManager.deleteOfficeLocation(reqMap);
			
			HttpSession session = request.getSession(false);
			session.setAttribute("isPRG", true);
			
			response.sendRedirect("PRGServlet?status=success&url=officeLocation.jsp&alertMessage="
					+ AlertMessages.requestDelete);
			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		doGet(request, response);
	}

}
