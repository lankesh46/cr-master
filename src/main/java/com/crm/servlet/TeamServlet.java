package com.crm.servlet;

import java.io.IOException;
import java.sql.DriverManager;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Connection;
import java.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.crm.utils.service.AlertMessages;

import com.crm.service.business.contract.RequestRespManager;

@WebServlet("/TeamServlet")
@MultipartConfig
public class TeamServlet extends HttpServlet
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	private Logger				log					= LoggerFactory.getLogger(getClass());

	@Inject
	private RequestRespManager	requestManager;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Map<String, String> reqMap = new HashMap<>();
		String action = request.getParameter("action").toString();
		log.debug("Action for companny profile is generated");
		
	
		
		if (action.equals("GENERATED"))
		{
			String name = request.getParameter("name");
			String[] member_empId = request.getParameterValues("member_empId");
			System.out.println(member_empId);
		
			String res_member_empId = "";
			
			for(int i = 0; i< member_empId.length; i++)
			{
				res_member_empId = res_member_empId + member_empId[i]+",";
			}
			
			String member_empId_concat = res_member_empId.concat("|");
			
			String member_empId_values = member_empId_concat.replace(",|", "");
			
			String org_level = request.getParameter("org_level");
			String locationId = request.getParameter("locationId");
			String email = request.getParameter("email");
			/*String address_street1 = request.getParameter("address_street1");
			String address_street2 = request.getParameter("address_street2");
			String city = request.getParameter("city");
			String state = request.getParameter("state");
			String zipcode = request.getParameter("zipcode");
			String country = request.getParameter("country");
			String comment = request.getParameter("comment");*/
			//		String status = request.getParameter("status");
			//		String active = request.getParameter("action");
			//
			//		String createdBy = request.getParameter("createdBy");
			//		String ip_address = "";
			
			reqMap.put("name", name);
			reqMap.put("member_empId", member_empId_values);
			reqMap.put("org_level", org_level);
			reqMap.put("locationId", locationId);
			reqMap.put("email", email);
			reqMap.put("Status", "active");
			reqMap.put("Active", "1");
			reqMap.put("createdBy", "Admin");
			reqMap.put("ip_address", request.getRemoteAddr());

			requestManager.registerTeam(reqMap);

			HttpSession session = request.getSession(false);
			session.setAttribute("isPRG", true);
			response.sendRedirect("PRGServlet?status=success&url=team.jsp&alertMessage="
					+ AlertMessages.requestSent);

		}
		
		else if(action.equals("delete"))
		{
			
			log.debug("Delete Action for Team is generated");
		
			String id = request.getParameter("id");
			
			reqMap.put("id", id);
			
			requestManager.deleteTeam(reqMap);
			
			HttpSession session = request.getSession(false);
			session.setAttribute("isPRG", true);
			
			response.sendRedirect("PRGServlet?status=success&url=team.jsp&alertMessage="
					+ AlertMessages.requestDelete);
			
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		doGet(request, response);
	}

}
