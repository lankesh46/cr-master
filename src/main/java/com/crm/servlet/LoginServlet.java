package com.crm.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.crm.service.business.contract.EAuthService;
import com.crm.views.SessionBean;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet
{
	private static final long	serialVersionUID	= 1L;

	@Inject
	private EAuthService		eAuthService;
	
	@Inject
	private SessionBean sessionBean;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String action = request.getParameter("action").toString();
		String userId = request.getParameter("username").toString();
		if (!action.isEmpty() && action.length() > 0 && action.equalsIgnoreCase("signIn"))
		{
			if (eAuthService.isAuthenticated(userId.toLowerCase()))
			{
				eAuthService.setEmpData(userId.toLowerCase());
				sessionBean.setEmployee(eAuthService.setEmpData(userId.toLowerCase()));
				response.sendRedirect("index.jsp");
				HttpSession session;
				session = request.getSession(true);
				session.setAttribute("userID", userId);
			}
			else
			{
				response.sendRedirect("login.jsp");

			}
		}
	}

}
